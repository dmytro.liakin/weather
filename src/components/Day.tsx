export type DayProps = {
    id: string,
    rain_probability: number,
    humidity: number,
    day: number,
    temperature: number,
    type: string,
}

export const Day = (props: DayProps) => {
    const date = new Date(props.day)
    const formatDate = new Intl.DateTimeFormat('ru', {weekday: 'long'}).format(date)

    return (
        <div className={`day ${props.type}`}>
            <p>{formatDate}</p>
            <span>{props.temperature}</span>
        </div>
    )
}