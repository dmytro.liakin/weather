import {Day} from './Day';
import forecastData from '../mock-data/forecast.json';

const dayLimit = 7;

export const Forecast = () => (
    <div className="forecast">
        {forecastData.slice(0, dayLimit).map((day) => <Day {...day} />)}
    </div>
);
