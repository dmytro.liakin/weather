import React from 'react';
import { Head, Filter, Forecast, CurrentWeather } from './components';

export const App: React.FC = () => {
   return (
        <main>
            <Filter />
            <Head />
            <CurrentWeather />
            <Forecast />
        </main>
   );
};
